#include <stdlib.h>
#include <stdio.h>
#include "core.h"

Core* newCore() {
	Core* core;
	//printf("%d", sizeof(Core));
	core = (Core*) malloc(sizeof(Core));
	core->longInt = 123;
	return core;
}

void destroyCore(Core* core) {
	free(core);
}
