/*
 * core.h
 *
 *  Created on: 8 abr. 2020
 *      Author: adrie
 */

#ifndef CORE_H_
#define CORE_H_

typedef struct core {
	long int longInt;	// 4
	long int long2;		// 4
} Core;

//CONTRUCTOR
Core* newCore();

//DESTRUCTOR
void destroyCore(Core* core);

#endif /* CORE_H_ */
