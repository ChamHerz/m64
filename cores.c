#include <stdlib.h>
#include "cores.h"

Cores* newCores() {
	Cores* cores;
	cores = (Cores*) malloc(sizeof(Cores));
	//cores->size = 268435456;
	cores->size = 43108864;
	cores->coreVector = (Core**) malloc(cores->size * sizeof(Core));
	return cores;
}

void destroyCores(Cores* cores) {
	free(cores);
}
