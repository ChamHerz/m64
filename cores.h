/*
 * cores.h
 *
 *  Created on: 8 abr. 2020
 *      Author: adrie
 */

#ifndef CORES_H_
#define CORES_H_

#include "core.h"

typedef struct cores {
	unsigned long long int size;
	Core** coreVector;
} Cores;

//CONTRUCTOR
Cores* newCores();

//DESTRUCTOR
void destroyCores(Cores* cores);


#endif /* CORES_H_ */
