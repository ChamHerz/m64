#include <stdio.h>
#include <conio.h>
#include "core.h"
#include "cores.h"

int indexCores = 0;
Cores* cores[128];

void upUp() {
	printf("Memoria %d MB \n", (indexCores + 1) * 1024);
	cores[indexCores] = newCores();
	for (long int indexCore = 1; indexCore < cores[indexCores]->size; indexCore++) {
		cores[indexCores]->coreVector[indexCore] = newCore();
	}
	++indexCores;
}

void upDown() {
	--indexCores;
	printf("Memoria %d MB \n", (indexCores + 1) * 1024);
	for (long int indexCore = 1; indexCore < cores[indexCores]->size; indexCore++) {
		destroyCore(cores[indexCores]->coreVector[indexCore]);
	}
	destroyCores(cores[indexCores]);
}

int main() {
	printf("inicia programa \n");
	char oneChar;
	int quit = 0;
	while(!quit) {
		oneChar = _getch();
		switch(oneChar) {
		case 0:
			oneChar = _getch();
		    if (oneChar == 107) {
		    	quit = 1; // Alt + F4
		    }
		    break;
		case -32:
			oneChar = _getch();
			if (oneChar == 72) upUp(); // Flecha abajo
			if (oneChar == 80) upDown(); // Flecha abajo
		}
	}

	return 0;
}
